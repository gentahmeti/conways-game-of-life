package main;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import main.Main.InitialLife;

public class InitialFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitialFrame() {
		
		super("Conway's Game of Life");
		
		Container cp = getContentPane();
		cp.setLayout(new GridLayout(7,1));

		createButton(cp, Main.InitialLife.BLINKER);
		createButton(cp, Main.InitialLife.TOAD);
		createButton(cp, Main.InitialLife.BEACON);
		createButton(cp, Main.InitialLife.PULSAR);
		createButton(cp, Main.InitialLife.GLIDER);
		createButton(cp, Main.InitialLife.LWSS);
		createButton(cp, Main.InitialLife.RANDOM);
		
		setSize(300,200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	/** Create buttons for different setting of the game.
	 * 
	 * @param cp The frames container
	 * @param life One of the options of the game as seen on Main.InitialLife enum.
	 */
	private void createButton(Container cp, final Main.InitialLife life) {
		
		((JButton)cp.add(new JButton(life==InitialLife.LWSS ? "LIGHT WEIGHT SPACESHIP" : life.toString()))).addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Main(life);
			}
		});
	}
	
	public static void main(String[] args) {
		new InitialFrame();
	}
}

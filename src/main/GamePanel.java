package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class GamePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1205307435957209541L;
	private Cell[][] cellTable;
	
	public GamePanel(Cell[][] cellTable) {
		
		this.cellTable = cellTable;
		setPreferredSize(new Dimension(501,501));
		setBackground(Color.WHITE);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		
		((Graphics2D)g).setStroke(new BasicStroke(1));
		for(int i=0; i<cellTable[0].length;i++) {
			
			for(int j=0; j<cellTable.length;j++) {
				
				if(cellTable[i][j].isAlive()) {
					
					drawLiveCell(g, i, j);
				} else {
					
					drawDeadCell(g, i, j);
				}
			}
		}
	}
	
	/** Subroutine for drawing a dead cell.
	 * 
	 * @param g The 2D context for drawing.
	 * @param x The x coordinate of the cell.
	 * @param y The y coordinate of the cell.
	 */
	private void drawDeadCell(Graphics g, int x, int y) {
		
		g.setColor(Color.WHITE);
		g.fillRect(10*x, 10*y, 10, 10);
		
		g.setColor(Color.BLACK);
		g.drawRect(10*x, 10*y, 10, 10);
	}
	
	/** Subroutine for drawing a live cell.
	 * 
	 * @param g The 2D context for drawing.
	 * @param x The x coordinate of the cell.
	 * @param y The y coordinate of the cell.
	 */
	private void drawLiveCell(Graphics g, int x, int y) {
		
		g.setColor(Color.BLACK);
		g.fillRect(10*x, 10*y, 10, 10);
		
		g.setColor(Color.GRAY);
		g.drawRect(10*x, 10*y, 10, 10);
	}
}
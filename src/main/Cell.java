package main;

import java.util.ArrayList;
import java.util.List;

public class Cell {
	
	private List<Cell> neighbours;
	private boolean isAlive;
	private int liveNeighbours;
	private int newStateLiveNeighbours;
	
	/** Create a new default cell. */
	public Cell() {
		neighbours = new ArrayList<Cell>();
	}
	
	/** Set the current cell alive and notify it's neighbours.
	 * @param alive A boolean to show the state of the cell.
	 */
	public void setAlive(boolean alive) {
		
		isAlive = alive;
		notifyNeighbours(isAlive);
	}
	
	/** Adds a new neighbour the list of neighbours.
	 * @param neighbour The new neighbour being added the list.
	 */
	public void addNeighbour(Cell neighbour) {
		
		neighbours.add(neighbour);
	}
	
	/** Check if the current cell will survive, or become alive according to the rules of the game.
	 * 
	 * Any live cell with fewer than two live neighbours dies, as if caused by under-population.
	 * Any live cell with two or three live neighbours lives on to the next generation.
	 * Any live cell with more than three live neighbours dies, as if by overcrowding.
     * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
	 */
	public void determineFate() {
	
		//We save the old state to see if any changes happened.
		boolean oldState = isAlive;
		
		//Is the cell alive?
		if(isAlive) {
			
			isAlive = liveNeighbours>=2 && liveNeighbours<=3;
		}
		else {
			
			isAlive = liveNeighbours==3;
		}
		
		//State changed?
		if(oldState!=isAlive) {
			
			notifyNeighbours(isAlive);
		}
	}
	
	/** Refreshes the old state to the new state calculated during the game. */
	public void refresh() {

		//Use the new data
		liveNeighbours = newStateLiveNeighbours;
	}
	
	/** Decrement the new state number of live neighbours. */
	public void neighbourDied() {
		
		newStateLiveNeighbours--;
	}
	
	/** Increment the new state number of live neighbours. */
	public void neighbourBirthed() {
		
		newStateLiveNeighbours++;
	}
	
	/** Returns a boolean indicating whether this cell is alive or dead. */
	public boolean isAlive() {
		
		return isAlive;
	}
	
	/** We use this method to notify all neighbours that this cells state has changed.
	 * 
	 * @param cellBirthed Was this cell born, or did it die?
	 */
	private void notifyNeighbours(boolean cellBirthed) {
		
		for(Cell cell : neighbours) {
			
			if(!cellBirthed) {
				cell.neighbourDied();
			}
			else {
				cell.neighbourBirthed();
			}			
		}
	}
}
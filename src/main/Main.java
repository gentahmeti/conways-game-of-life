package main;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 922684098203457246L;

	public static enum InitialLife {
		BLINKER, TOAD, BEACON, PULSAR, GLIDER, LWSS, RANDOM
	}
	
	private Cell[][] cellTable = new Cell[50][50];
	private JPanel panel;
	private double birthChance = 0.1;

	/** Constructor method for the frame
	 * 
	 * @param life The initial population as seen by the InitialLife enum
	 */
	public Main(InitialLife life) {

		super(life==InitialLife.LWSS ? "LGHT WEIGHT SPACESHIP" : life.toString());

		this.populateCellTable(this.cellTable, 0, 0);

		panel = new GamePanel(cellTable);
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(panel, BorderLayout.CENTER);

		//Set the frame to be on the center of the screen.
		setLocationRelativeTo(null);
		pack();

		createInitialPopulation(life);
		setVisible(true);
	
		//Play the game in another thread.
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				play();
			}
		}).start();
	}

	/** Creates a population based on it's initial life
	 * 
	 * @param life The initial form of the population
	 */
	private void createInitialPopulation(InitialLife life) {
		
		switch (life) {
		case BLINKER:
			createBlinker();
			break;
		case TOAD:
			createToad();
			break;
		case BEACON:
			createBeacon();
			break;
		case PULSAR:
			createPulsar();
			break;
		case GLIDER:
			createGlider();
			break;
		case LWSS:
			createLWSS();
			break;
		default:
			createInitialPopulation();
			break;
		}
	}

	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createBlinker() {
		
		cellTable[4][4].setAlive(true);
		cellTable[4][5].setAlive(true);
		cellTable[4][6].setAlive(true);
	}
	
	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createToad() {
		
		cellTable[4][4].setAlive(true);
		cellTable[5][4].setAlive(true);
		cellTable[6][4].setAlive(true);
		
		cellTable[5][3].setAlive(true);
		cellTable[6][3].setAlive(true);
		cellTable[7][3].setAlive(true);
	}
	
	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createBeacon() {
		
		cellTable[4][4].setAlive(true);
		cellTable[5][4].setAlive(true);
		cellTable[4][5].setAlive(true);
		cellTable[5][5].setAlive(true);
		
		cellTable[6][6].setAlive(true);
		cellTable[7][6].setAlive(true);
		cellTable[6][7].setAlive(true);
		cellTable[7][7].setAlive(true);
	}
	
	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createGlider() {
		
		cellTable[5][4].setAlive(true);
		
		cellTable[6][5].setAlive(true);
		
		cellTable[4][6].setAlive(true);
		cellTable[5][6].setAlive(true);
		cellTable[6][6].setAlive(true);
	}
	
	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createLWSS() {
		
		cellTable[5][4].setAlive(true);
		cellTable[6][4].setAlive(true);
		cellTable[7][4].setAlive(true);
		cellTable[8][4].setAlive(true);
		
		cellTable[4][5].setAlive(true);
		cellTable[8][5].setAlive(true);
		
		cellTable[8][6].setAlive(true);
		
		cellTable[4][7].setAlive(true);
		cellTable[7][7].setAlive(true);
	}
	
	/** Creates a population which is a well known form
	 * 	As seen here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns
	 */
	private void createPulsar() {
		
		cellTable[6][4].setAlive(true);
		cellTable[7][4].setAlive(true);
		cellTable[8][4].setAlive(true);
		cellTable[12][4].setAlive(true);
		cellTable[13][4].setAlive(true);
		cellTable[14][4].setAlive(true);
		
		cellTable[4][6].setAlive(true);
		cellTable[9][6].setAlive(true);
		cellTable[11][6].setAlive(true);
		cellTable[16][6].setAlive(true);
		
		cellTable[4][7].setAlive(true);
		cellTable[9][7].setAlive(true);
		cellTable[11][7].setAlive(true);
		cellTable[16][7].setAlive(true);
		
		cellTable[4][8].setAlive(true);
		cellTable[9][8].setAlive(true);
		cellTable[11][8].setAlive(true);
		cellTable[16][8].setAlive(true);

		cellTable[6][9].setAlive(true);
		cellTable[7][9].setAlive(true);
		cellTable[8][9].setAlive(true);
		cellTable[12][9].setAlive(true);
		cellTable[13][9].setAlive(true);
		cellTable[14][9].setAlive(true);
		
		cellTable[6][11].setAlive(true);
		cellTable[7][11].setAlive(true);
		cellTable[8][11].setAlive(true);
		cellTable[12][11].setAlive(true);
		cellTable[13][11].setAlive(true);
		cellTable[14][11].setAlive(true);
		
		cellTable[4][12].setAlive(true);
		cellTable[9][12].setAlive(true);
		cellTable[11][12].setAlive(true);
		cellTable[16][12].setAlive(true);
		
		cellTable[4][13].setAlive(true);
		cellTable[9][13].setAlive(true);
		cellTable[11][13].setAlive(true);
		cellTable[16][13].setAlive(true);
		
		cellTable[4][14].setAlive(true);
		cellTable[9][14].setAlive(true);
		cellTable[11][14].setAlive(true);
		cellTable[16][14].setAlive(true);
		
		cellTable[6][16].setAlive(true);
		cellTable[7][16].setAlive(true);
		cellTable[8][16].setAlive(true);
		cellTable[12][16].setAlive(true);
		cellTable[13][16].setAlive(true);
		cellTable[14][16].setAlive(true);
	}
	
	/** Creates a population in random where each cell has a 10% chance of becoming alive.
	 */
	private void createInitialPopulation() {

		for(int i=0; i<cellTable[0].length; i++) {

			for(int j=0; j<cellTable.length; j++) {

				if(Math.random()<birthChance) {

					cellTable[i][j].setAlive(true);
				}
			}
		}

		panel.repaint();
	}

	/** Refresh the old state. */
	private void refreshAll() {
		for(int i=0; i<cellTable[0].length; i++) {

			for(int j=0; j<cellTable.length; j++) {

				cellTable[i][j].refresh();
			}
		}
	}

	/** A recursive method which populates the table of cells.
	 * @param cellTable The table which is being filled.
	 * @param x The current x coordinate of the cell.
	 * @param y The current y coordinate of the cell.
	 * @return The currently created cell.
	 */
	private Cell populateCellTable(Cell[][] cellTable, int x, int y) {

		cellTable[x][y] = new Cell();

		//Possible neighbour indices
		int[][] pni = {{x+1,y-1},{x+1,y},{x+1,y+1},{x-1,y-1},{x-1,y},{x-1,y+1},{x,y-1},{x,y+1}};
		
		for(int i=0;i<pni.length;i++) {

			//Is the index within the bounds of the table.
			if(!isIndexOutOfBounds(pni[i][0], pni[i][1])) {
				
				//Is this neighbour initialized?
				if(cellTable[pni[i][0]][pni[i][1]]!=null) {
					//If so add it to the list of neighbours for the current cell.
					cellTable[x][y].addNeighbour(cellTable[pni[i][0]][pni[i][1]]);
				}
				else {
					//If not create it using this recursive method.
					cellTable[x][y].addNeighbour(populateCellTable(cellTable, pni[i][0], pni[i][1]));
				}
			}
		}

		return cellTable[x][y];
	}

	/** Checks to see if the given coordinate is within the bounds of the Cell table.
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @return true if the given coordinates are out of bounds, false otherwise.
	 */
	private boolean isIndexOutOfBounds(int x, int y) {

		return x<0 || y<0 || x>=cellTable[0].length || y>=cellTable.length;
	}

	/** Start the game
	 */
	private void play() {
		
		while(true) {

			refreshAll();
			for(int i=0; i<cellTable[0].length; i++) {
				for(int j=0; j<cellTable.length; j++) {
					
					cellTable[i][j].determineFate();
				}
			}

			panel.repaint();
			try {
				Thread.sleep(200);
			}
			catch(InterruptedException ex) {
			}
		}
	}
}	